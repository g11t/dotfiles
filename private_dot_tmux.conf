set -g default-terminal "screen-256color"
set -g history-limit 100000

# Change hotkey to Control-A
unbind C-b
set -g prefix C-a

# Reduce repeat-time to avoid tmux thinking I want to keep holding down the prefix...
set-option -g repeat-time 500

# Bind a to go to beginning of line (ctrl-a expected behaviour)
bind a send-prefix

# Change active window to yellow
set-window-option -g window-status-current-style bg=yellow

# Quick way to turn off synchronize panes (Control-A Option-A toggles it)
bind-key M-a set-window-option synchronize-panes

# prefix + r to reload tmux config
unbind r
bind r source-file ~/.tmux.conf

# horizontal and vertical splits
unbind |
bind | split-window -h
unbind -
bind - split-window

# tile all windows
unbind =
bind = select-layout tiled

# Copy mode works as Vim
unbind [
bind [ copy-mode

# Enable mouse
set -g mouse

bind-key -r M-Right resize-pane -R 2
bind-key -r M-Left resize-pane -L 2
bind-key -r M-Up resize-pane -U 2
bind-key -r M-Down resize-pane -D 2

# Faster resize
# unbind C-Right
# bind M-Right resize-pane -R 8
# unbind M-Left
# bind M-Left resize-pane -L 8
# unbind M-Up
# bind M-Up resize-pane -U 4
# unbind M-Down
# bind M-Down resize-pane -D 4

# Start numbering at 1
set -g base-index 1
set -g renumber-windows on
set -s escape-time 0

# cycle through panes
unbind ^A
bind ^A select-pane -t :.+

# kill
unbind k
bind k confirm-before "kill-window"

# Copy-paste integration
# set-option -g default-command "reattach-to-user-namespace -l zsh"

# Use vim keybindings in copy mode
setw -g mode-keys vi

# Setup 'v' to begin selection as in Vim
# bind-key -t vi-copy v begin-selection
# bind-key -t vi-copy y copy-pipe "reattach-to-user-namespace pbcopy"

# Update default binding of `Enter` to also use copy-pipe
# unbind -t vi-copy Enter
# bind-key -t vi-copy Enter copy-pipe "reattach-to-user-namespace pbcopy"

# Use xterm keys so CTRL-left/right work to jump words
set-window-option -g xterm-keys on

# Bind ']' to use pbpaste
# bind ] run "reattach-to-user-namespace pbpaste | tmux load-buffer - && tmux paste-buffer"

# Use this to find the pane with a particular process ID
bind-key W command-prompt -p "Switch to pane with pid:" "run-shell 'pane=\$(ps eww %% | sed \"1d; s/^.*TMUX_PANE=//;s/ .*//\"); [[ -z \$pane ]] && tmux display-message \"could not find pid\" || tmux switch-client -t \$pane'"

bind-key -T copy-mode-vi v send -X begin-selection
##bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "xclip -selection clipboard"
##bind-key -T copy-mode-vi y send-keys -X copy-pipe "xclip -selection clipboard"
#bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "xclip -selection clipboard"
#bind-key -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel "xclip -selection clipboard"
##bind-key -T copy-mode-vi Enter send-keys -X copy-pipe "xclip -selection clipboard"
##unbind -T copy-mode-vi MouseDragEnd1Pane

set -g @yank_action 'copy-pipe'

# Source theme
if-shell "test -e ~/.config/tmux/theme.conf" "source ~/.config/tmux/theme.conf" "source ~/.config/tmux/themes/default.conf"

## Plugins
source ~/.config/tmux/plugins.conf
